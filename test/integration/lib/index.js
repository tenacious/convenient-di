const helper = require('../../__fixtures/test-helper').create();
const path = require('path');
var expect = require('expect.js');
const delay = require('await-delay');
//const printOpenHandles = require('why-is-node-running');

describe(helper.testName(__filename, 4), function() {

  this.timeout(15000);

  it('tests initializing and starting a service, we ensure the example1 service has correct config and defaults', async () => {

    let container = require('convenient-di').Container.create({
      projectPath: path.resolve(__dirname, '../../__fixtures/example-project')
    });

    await container.register();
    await container.resolve();
    await container.start();
    await delay(2000);

    expect(container.services.example1.config.defaultSetting).to.be(true);
    expect(container.services.example1.config.services.example1.customSetting1).to.be(false);
    expect(container.services.example1.config.services.example1.customSetting2).to.be(true);

    await container.stop();
    //await delay(2000);
    //printOpenHandles();
  });

  it('tests initializing and starting a server - failure due to circular dependencies', async () => {

    let caughtError = false;

    try{
      let container = require('convenient-di').Container.create({
        projectPath: path.resolve(__dirname, '../../__fixtures/example-project-circular-services-dependencies')
      });
    } catch(e) {
      expect(e.message).to.be('Circular dependency detected: between example1 and example2 services detected, please check the service constructors');
      caughtError = true;
    }

    expect(caughtError).to.be(true);
  });

  it('tests initializing and starting a server - failure due to circular dependency to self', async () => {

    let caughtError = false;

    try{
      let container = require('convenient-di').Container.create({
        projectPath: path.resolve(__dirname, '../../__fixtures/example-project-circular-services-dependencies-self')
      });
    } catch(e) {
      expect(e.message).to.be('Circular dependency detected: the example1 service cannot depend on itself, please check the service constructors');
      caughtError = true;
    }

    expect(caughtError).to.be(true);
  });
});
