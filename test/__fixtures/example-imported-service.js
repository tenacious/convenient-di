const Service = require('convenient-di').Service;

class ExampleService extends Service {
  constructor(config, common){
    super(config, common);
  }
}

ExampleService.Imported = class extends Service {
  constructor(config, common){
    super(config, common);
  }
}

module.exports = ExampleService;
