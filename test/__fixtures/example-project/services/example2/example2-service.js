const Service = require('convenient-di').Service;

class Example2Service extends Service {

  constructor(config, common){
    super(config, common);
  }

  async init(){

  }

  async start(){

  }

  async stop(){

  }

  defaults(){

  }

  async initNode(){
    this.common.logger.info('service2 init node');
  }

  async doSomething(msg){
    msg.example2DidSomething = true;
    return msg;
  }
}

module.exports = Example2Service;
