const helper = require('../../../__fixtures/test-helper').create();
const path = require('path');
const _expect = require('chai').expect;
const sinon = require('sinon');

let ConvenientDI;

describe(helper.testName(__filename, 4), function() {
  beforeEach(function beforeEach(){
    ConvenientDI = require('convenient-di');
  });

  afterEach(function afterEach(){
    sinon.restore()
    delete require.cache[require.resolve('convenient-di')];
  })

  function mockCommon(){
    let common = {};
    common.utils = require('plebs').Utils.create();
    common.logger = require('plebs').Logger.create();
    common.crypto = require('plebs').Crypto.create();
    return common;
  }

  it('creating container using constructor', () => {
    const container = new ConvenientDI.Container({
      projectPath: path.resolve(__dirname, '../../../__fixtures/example-project')
    });
    _expect(container.common).to.not.be.null
    _expect(container.config).to.not.be.null
  });

  it('tests initializing and returning container', () => {
    ConvenientDI.Container.create({
      projectPath: path.resolve(__dirname, '../../../__fixtures/example-project')
    }, mockCommon());

    _expect(ConvenientDI.Container.instance()).to.not.be.null
  });

  it('tests initializing the container: good path', () => {
    let container = ConvenientDI.Container.create({
      projectPath: path.resolve(__dirname, '../../../__fixtures/example-project')
    }, mockCommon());

    _expect(container.config.services['example1'].dependencies).to.eql(['config', 'common', 'example2']);
    _expect(container.config.services['example2'].dependencies).to.eql(['config', 'common']);
  });

  it('tests imported service without classname', () => {
    const container = ConvenientDI.Container.create({
      projectPath: path.resolve(__dirname, '../../../__fixtures/example-project')
    }, mockCommon());

    const config = container.setupImportedServices({
      services: {
        '$test-service': {
          path: path.resolve(__dirname, '../../../__fixtures/example-imported-service.js')
        }
      }
    });

    _expect(config).to.have.property('services')
      .that.has.property('testService')
      .that.is.not.null
  });

  it('tests imported service with classname', () => {
    const container = ConvenientDI.Container.create({
      projectPath: path.resolve(__dirname, '../../../__fixtures/example-project')
    }, mockCommon());

    const config = container.setupImportedServices({
      services: {
        '$test-service::Imported': {
          path: path.resolve(__dirname, '../../../__fixtures/example-imported-service.js')
        }
      }
    });

    _expect(config).to.have.property('services')
      .that.has.property('testService')
      .that.is.not.null
  });

  it('tests service merging', () => {
    const container = ConvenientDI.Container.create({
      projectPath: path.resolve(__dirname, '../../../__fixtures/example-project')
    }, mockCommon());

    container.register()
    container.resolve()

    const merged = {};
    container.merge(merged);

    _expect(merged).to.have.property('example1');
    _expect(merged).to.have.property('example2');
  });

  it('tests service initialization', async function (){
    const container = ConvenientDI.Container.create({
      projectPath: path.resolve(__dirname, '../../../__fixtures/example-project')
    }, mockCommon());

    container.register()
    container.resolve()

    for(const service of Object.values(container.services)){
      sinon.spy(service, '__init')
    }

    await container.init()

    for(const service of Object.values(container.services)){
      _expect(service.__init.calledOnce).to.be.true
    }
  });

  it('tests service configuration', async function (){
    const container = ConvenientDI.Container.create({
      projectPath: path.resolve(__dirname, '../../../__fixtures/example-project')
    }, mockCommon());

    const config = {};
    container.configureService('test', config);
    console.log(config);
  });

  it('tests container creation from container', async function (){
    const container = ConvenientDI.Container.create({
      projectPath: path.resolve(__dirname, '../../../__fixtures/example-project')
    }, mockCommon());

    const subContainer = container.create({
      projectPath: path.resolve(__dirname, '../../../__fixtures/example-project')
    }, mockCommon());

    _expect(subContainer).to.not.be.null
  });

  it('tests throw from setup service config', async function (){
    const container = ConvenientDI.Container.create({
      projectPath: path.resolve(__dirname, '../../../__fixtures/example-project')
    }, mockCommon());

    sinon.stub(container.common.utils, 'getConstructorArgumentNamesFromPath').throws(Error('test error'));

    const bound = container.setupServicesConfig.bind(container, {
      projectPath: path.resolve(__dirname, '../../../__fixtures/example-project'),
      services: {}
    });
    _expect(bound).to.throw(Error, /^Failure.*?: test error$/)
  });
});
