const helper = require('../../../__fixtures/test-helper').create();
const path = require('path');
const _expect = require('chai').expect;
const sinon = require('sinon');

let ConvenientDI;

describe(helper.testName(__filename, 4), function() {
  beforeEach(function beforeEach(){
    ConvenientDI = require('convenient-di');
    sinon.spy(ConvenientDI.Service.prototype, '__init')
    sinon.spy(ConvenientDI.Service.prototype, '__start')
    sinon.spy(ConvenientDI.Service.prototype, '__stop')
    this.mockService = class MockService extends ConvenientDI.Service{};
  });

  afterEach(function afterEach(){
    sinon.restore()
    delete require.cache[require.resolve('convenient-di')];
  });

  function mockCommon(){
    let common = {};
    common.utils = require('plebs').Utils.create();
    common.logger = require('plebs').Logger.create();
    common.crypto = require('plebs').Crypto.create();
    return common;
  }

  it('tests service init', async function(){
    const svc = new this.mockService({}, mockCommon());
    await svc.__init();
    _expect(svc.__init.calledOnce).to.be.true

    svc.init = sinon.fake();
    await svc.__init();
    _expect(svc.__init.calledTwice).to.be.true
    _expect(svc.init.calledOnce).to.be.true
  });

  it('tests service start', async function(){
    const svc = new this.mockService({}, mockCommon());
    await svc.__start();
    _expect(svc.__start.calledOnce).to.be.true

    svc.start = sinon.fake();
    await svc.__start();
    _expect(svc.__start.calledTwice).to.be.true
    _expect(svc.start.calledOnce).to.be.true
  });

  it('tests service stop', async function(){
    const svc = new this.mockService({}, mockCommon());
    await svc.__stop();
    _expect(svc.__stop.calledOnce).to.be.true

    svc.stop = sinon.fake();
    await svc.__stop();
    _expect(svc.__stop.calledTwice).to.be.true
    _expect(svc.stop.calledOnce).to.be.true
  });

  it('tests service stop', async function(){
    const svc = new this.mockService({}, mockCommon());
    const firstRound = svc.getSetting('blah', false);
    svc.config.blah = true;
    const secondRound = svc.getSetting('blah', false);

    _expect(firstRound).to.be.false;
    _expect(secondRound).to.be.true;
  });
});
