const finder = require('find-package-json');
const path = require('path');
const EventEmitter = require('events').EventEmitter;

const Container = require("./container");

class Service extends EventEmitter {
  constructor(config, common){
    super();
    this.common = common;
    this.config = common.utils.clone(config);
    if (this.defaults) this.defaults(config);
  }

  async __init(){
    if (typeof this.init == 'function') await this.init();
  }

  async __start(){
    if (typeof this.start == 'function') await this.start();
  }

  async __stop(){
    if (typeof this.stop == 'function') await this.stop();
  }

  getSetting(key, defaultSetting){
    return this.config[key] || defaultSetting;
  }
}

class ImportedContainer extends Service {

  resolve(){
    const classPath = this.classDirName();
    var f = finder(classPath);
    return path.dirname(f.next().filename);
  }

  getImportedConfig(config){
    const camelCase = require('camelcase');
    let modulePath;
    let packageName;
    try{
      modulePath = this.resolve();//get the module base path
      packageName = require(`${modulePath}${path.sep}package.json`).name;
    }catch(e){
      throw new Error(`could not resolve imported service module path on: ${modulePath}: ${e.message}`);
    }
    let containerConfig = config.services[camelCase(packageName)].config;
    containerConfig.projectPath = `${modulePath}${path.sep}lib`;
    return containerConfig;
  }

  constructor(config, common){
    super(config, common);
    this.config = this.getImportedConfig(config);
  }

  static create(config, common){
    return new ImportedContainer(config, common);
  }

  async init() {
    if(!Container.instance())
        throw new Error("Cannot use imported container without root container");

    this.container = new Container(this.config);
    await this.container.register();
    await this.container.resolve();
    this.container.merge(this);
  }

  async start() {
    await this.container.start();
  }

  async stop() {
    if (this.container) await this.container.stop();
  }

  defaults(){

  }
}

module.exports.Service = Service;
module.exports.ImportedContainer = ImportedContainer;
