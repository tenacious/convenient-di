const {ContainerBuilder, Reference} = require('node-dependency-injection');
const container_root = {};
const path = require('path');
const _ = require('lodash');
const camelCase = require('camelcase');

class Container extends require('events').EventEmitter {

  constructor(config, common){
    super();
    this.services = {};
    this.builder = new ContainerBuilder();
    this.common = common || this.createCommon(config);
    this.config = this.defaults(config);
  }

  createCommon(config){
    let plebs = require('plebs');
    return {
      logger:plebs.Logger.create(config),
      utils:plebs.Utils.create(config),
      crypto:plebs.Crypto.create(config)
    };
  }

  create(config, common){
    return new Container(config, common);
  }

  registerInstance(key, instance){
    let definition = this.builder.register(key);
    definition.synthetic = true;
    this.builder.set(key, instance);
  }

  configureService(serviceName, config){
    if (!config.services) config.services = {};
    if (!config.services[serviceName]) config.services[serviceName] = {};
    if (!config.services[serviceName].path) config.services[serviceName].path = `./${serviceName}/${serviceName}-service.js`;
    if (!config.services[serviceName].dependencies) config.services[serviceName].dependencies = ['config', 'common'];
  }

  register(){
    this.registerInstance('config', this.config);
    this.registerInstance('common', this.common);
    Object.keys(this.config.services).forEach((serviceName) =>{
      let serviceConfig = this.config.services[serviceName];
      let toRegister = serviceConfig.className?
        require(serviceConfig.path)[serviceConfig.className]:
        require(serviceConfig.path);

      var registered = this.builder.register(serviceName, toRegister);

      serviceConfig.dependencies.forEach((dependencyName)=>{
        registered.addArgument(new Reference(dependencyName));
      });
    });
  }

  resolve(){
    for (let serviceName of Object.keys(this.config.services)){
      this.services[serviceName] = this.builder.get(serviceName);
    }
  }

  merge(service){
    Object.keys(this.services).forEach(serviceName => {
      service[serviceName] = this.services[serviceName];
    });
  }

  async init(){
    for (let serviceName of Object.keys(this.config.services)){
      await this.services[serviceName].__init();
    }
  }

  async start(){
    for (let serviceName of Object.keys(this.config.services)){
      await this.services[serviceName].__start();
    }
  }

  async stop(){
    for (let serviceName of Object.keys(this.config.services)){
      await this.services[serviceName].__stop();
    }
  }

  detectCircularDependencies(config){
    for (let serviceName in config.services) {
      for (let dependency of config.services[serviceName].dependencies) {
        if (dependency == serviceName)
          throw new Error(`Circular dependency detected: the ${serviceName} service cannot depend on itself, please check the service constructors`);
        if (config.services[dependency] && config.services[dependency].dependencies.indexOf(serviceName) > -1)
          throw new Error(`Circular dependency detected: between ${serviceName} and ${dependency} services detected, please check the service constructors`);
      }
    }
  }

  setupImportedServices(config){

    Object.keys(config.services).forEach((serviceConfigName) => {
      if (serviceConfigName.indexOf('$') != 0) return;
      var containerModuleName = serviceConfigName.substring(1);
      let className;
      if (serviceConfigName.indexOf('::') > -1){
        containerModuleName = containerModuleName.split('::')[0];
        className = serviceConfigName.split('::')[1];
      }
      let serviceConfig = config.services[serviceConfigName];
      let containerModuleAliasName = camelCase(containerModuleName);
      let moduleAbsolutePath = serviceConfig.path || require.resolve(containerModuleName);

      let containerModuleAliasConfig = {
        path:moduleAbsolutePath,
        className,
        //overwrite the path with the actual service path
        dependencies:this.common.utils.getConstructorArgumentNamesFromPath(moduleAbsolutePath, className),
        config:serviceConfig.config
      };

      config.services[containerModuleAliasName] = containerModuleAliasConfig;
      //remove the $ configuration
      delete config.services[serviceConfigName];
    });
    return config;
  }

  setupServicesConfig(config){
    if (!config.services) config.services = {};
    let servicesPath = path.resolve(config.projectPath, './services');
    let servicesFolders = this.common.utils.folderExists(servicesPath) ?
      this.common.utils.getDirectories(servicesPath) : [];

    servicesFolders.forEach((serviceFolder) => {
      let serviceName = serviceFolder.name;
      try{
        if (typeof config.services[serviceName] !== 'object') config.services[serviceName] = {};
        config.services[serviceName].path = path.resolve(servicesPath, `./${serviceName}/${serviceName}-service.js`);
        config.services[serviceName].dependencies = this.common.utils.getConstructorArgumentNamesFromPath(config.services[serviceName].path);
      } catch(e) {
        throw new Error(`Failure configuring service ${serviceName}: ${e.message}`);
      }
    });

    try{
      let defaultsConfig = require(path.resolve(config.projectPath, './config.js'));
      _.defaultsDeep(config, defaultsConfig);
    } catch(e){
      //do nothing
    }

    this.setupImportedServices(config);
    this.detectCircularDependencies(config);
  }

  defaults(config){
    var defaultsConfig = this.common.utils.clone(config);
    this.setupServicesConfig(defaultsConfig);
    return defaultsConfig;
  }

  static instance(){
    return container_root.instance;
  }

  static create(config, common){
    container_root.instance = new Container(config, common);
    return container_root.instance;
  }
}

module.exports = Container;
