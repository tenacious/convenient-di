module.exports = {
  Service:require('./lib/services/service-base').Service,
  ImportedContainer:require('./lib/services/service-base').ImportedContainer,
  Container:require('./lib/services/container')
};
